<?php
    $b = 0;
    if (!isset($_COOKIE["auth"])) {
        $b = 1;
    }
?>
<script>
    if (<?php echo $b; ?>) {
        window.location.replace("../403.php");
    }
</script>

<?php
    include("layouthead.php");
    include("headerOther.php");
?>
	<main class="main_hobby">
        <div class="left-block">
            <div class="user_left-block">
                <?php 
                    //error_reporting(E_ERROR | E_PARSE);
                    $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
					$name = substr($url, 30);
                    $link = mysqli_connect('localhost', 'root', '', 'pairs');
                    if(mysqli_connect_errno()){  
                        echo mysqli_connect_error();  
                    }
                    $cookie = $_COOKIE["auth"];
                    $results = mysqli_query($link, "SELECT photo FROM users WHERE user_name = '$name'");
                    $index = 0;
                    $images = [];
                    while($row = mysqli_fetch_array($results))
                    {
                        $images[$index] = $row["photo"];
                        $index++;
                    }
                    $image = $images[0];
                    $imageAvatar = "";
                    for ($i = 0; $i < strlen($image); $i++) {
                        $imageAvatar .= $image[$i];
                    }
                    $style = "background: url(\"$imageAvatar\");";
                    echo "<img class=\"user_photo\", src='../$imageAvatar'></div>";
                ?>
                <div class="user_nickname">
                    <?php
                        echo $name;
                    ?>
                </div>
            </div>
            <div class="right_block">
                <p class="user_info-block-text">Пол: 
                    <?php
                        $link = mysqli_connect('localhost', 'root', '', 'pairs');

                        if(mysqli_connect_errno()){  
                            echo mysqli_connect_error();  
                        }
                        $cookie = $_COOKIE["auth"];
                        $query = "SELECT gender_name FROM genders WHERE id_gender = (SELECT id_gender FROM usersgenders WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$name'))";
                        $result = mysqli_query($link, $query);
                        $index = 0;
                        $gender_name = [];
                        while($row = mysqli_fetch_array($result))
                        {
                            $gender_name[$index] = $row["gender_name"];
                            $index++;
                        }
                        if (count($gender_name) > 0) {
                            echo $gender_name[0];
                        } else {
                            echo "мужской";
                        }
                        mysqli_close($link);
                    ?>
                </p>
                <p class="user_info-block-text">Возраст: 
                    <?php
                        $link = mysqli_connect('localhost', 'root', '', 'pairs');

                        if(mysqli_connect_errno()){  
                            echo mysqli_connect_error();  
                        }
                        $query = "SELECT age FROM users WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$name')";
                        $result = mysqli_query($link, $query);
                        $index = 0;
                        $age = [];
                        while($row = mysqli_fetch_array($result))
                        {
                            $age[$index] = $row["age"];
                            $index++;
                        }
                        echo $age[0];
                        mysqli_close($link);
                    ?>
                </p>
                <p class="user_info-block-text">Хобби: 
                    <?php
                        $link = mysqli_connect('localhost', 'root', '', 'pairs');

                        if(mysqli_connect_errno()){  
                            echo mysqli_connect_error();  
                        }
                        
                        $cookie = $_COOKIE["auth"];
                        $query = "SELECT id_hobby FROM usershobbies WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$name')";
                        $result = mysqli_query($link, $query);
                        $index = 0;
                        $hobbyids = [];
                        while($row = mysqli_fetch_array($result))
                        {
                            $hobbyids[$index] = $row["id_hobby"];
                            $index++;
                        }
                        $index = 0;
                        $usershobby = [];
                        for($i = 0; $i < count($hobbyids); $i++) {
                            $query = "SELECT hobby_name FROM hobbies WHERE id_hobby = $hobbyids[$i]";
                            $result = mysqli_query($link, $query);
                            while($row = mysqli_fetch_array($result))
                            {
                                $usershobby[$index] = $row["hobby_name"];
                                $index++;
                            }
                        }
                        for($i = 0; $i < count($usershobby); $i++) {
                            if ($i != count($usershobby) - 1) {
                                echo $usershobby[$i] . ", \n";
                            } else {
                                echo $usershobby[$i];
                            }
                        }
                        mysqli_close($link);
                    ?>
                </p>
                <p class="user_info-block-text">Лайки:
                    <?php
                        $link = mysqli_connect('localhost', 'root', '', 'pairs');

                        if(mysqli_connect_errno()){  
                            echo mysqli_connect_error();  
                        }
                        $cookie = $_COOKIE["auth"];
                        $query = "SELECT likes FROM users WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$name')";
                        $result = mysqli_query($link, $query);
                        $index = 0;
                        $likes = [];
                        while($row = mysqli_fetch_array($result))
                        {
                            $likes[$index] = $row["likes"];
                            $index++;
                        }
                        echo $likes[0];
                        mysqli_close($link);
                    ?>
                </p>
            </div>
            <?php
                echo "<form method=\"post\" style=\"margin-top: 40px;\">";
                echo    "<input type=\"text\" name=\"like\" value=\"НРАВИТСЯ?\" readonly>";
                echo "<div class=\"hobby__form-button\">";
                echo    "<input type=\"submit\" value=\"ЛАЙКНУТЬ?\">";
                echo "</div>";
                echo "</form>";
                $b = $_SERVER["REQUEST_METHOD"] == "POST";
                if ($b) { // проверяем, что форма отправлена методом POST
                    $like = $_POST['like'];
                    // использовать значение переменной $like здесь по своему усмотрению
                    $link = mysqli_connect('localhost', 'root', '', 'pairs');
                    $result = mysqli_query($link, "SELECT user_liked FROM userslikes WHERE user_name = '$cookie' AND user_liked = '$name'");
                    $index = 0;
                    $user_liked = [];
                    while($row = mysqli_fetch_array($result))
                    {
                        $user_liked[$index] = $row["user_liked"];
                        $index++;
                    }
                    if (count($user_liked) == "0") {
                        $result = mysqli_query($link, "INSERT INTO userslikes (user_name, user_liked) VALUES ('$cookie', '$name')");
                        $result2 = mysqli_query($link, "SELECT likes FROM users WHERE user_name = '$name'");
                        $index = 0;
                        $user_liked = [];
                        while($row = mysqli_fetch_array($result2))
                        {
                            $user_liked[$index] = $row["likes"];
                            $index++;
                        }
                        $new_likes = $user_liked[0] + 1;
                        $result3 = mysqli_query($link, "UPDATE users SET likes = $new_likes WHERE user_name = '$name'");
                    } else {
                        $result = mysqli_query($link, "DELETE FROM userslikes WHERE user_name = '$cookie' AND user_liked = '$name'");
                        $result2 = mysqli_query($link, "SELECT likes FROM users WHERE user_name = '$name'");
                        $index = 0;
                        $user_liked = [];
                        while($row = mysqli_fetch_array($result2))
                        {
                            $user_liked[$index] = $row["likes"];
                            $index++;
                        }
                        $new_likes = $user_liked[0] - 1;
                        $result3 = mysqli_query($link, "UPDATE users SET likes = $new_likes WHERE user_name = '$name'");
                    }
                    mysqli_close($link);
                }
            ?>
            <script>
                if (<?php echo $b ?>) {
                    window.location.replace("../profile.php");
                    window.location.replace("../profileother.php/<?php echo $name ?>");
                }
            </script>    
        </div>
	</main>
<?php
    include("footerOther.html");
    include("layoutfoot.html");
?>