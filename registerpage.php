<?php
    include("layouthead.php");
    include("header.php");
?>

    <main class="main__register">
        <form class="register__form">
            <div class="register__form-area">
                <div class="input" style="margin-top: 25px">
                    <label>
                        <input class="register__form-text" type="text" name="nickname" placeholder="Никнейм"/>
                    </label>
                </div>
                <div class="input">
                    <label>
                        <input class="register__form-text" type="text" name="email" placeholder="Email"/>
                    </label>
                </div>
                <div class="input">
                    <label>
                        <input class="register__form-text" type="text" name="age" placeholder="Возраст"/>
                    </label>
                </div>
                <select class="input" style="padding-top: 0" id="selectGender" name="sex">
                    <option value="0">Пол</option>
                    <?php
                    $link = mysqli_connect('localhost', 'root', '', 'pairs');

                    if(mysqli_connect_errno()){
                        echo mysqli_connect_error();
                    }

                    $query = "SELECT gender_name FROM genders";
                    $result = mysqli_query($link, $query);
                    $index = 0;
                    $genders = [];
                    while($row = mysqli_fetch_array($result))
                    {
                        $genders[$index] = $row["gender_name"];
                        $index++;
                    }
                    foreach($genders as $gender) {
                        echo "<option>$gender</option>";
                    }
                    mysqli_close($link);
                    ?>
                </select>
                <div class="input">
                    <label>
                        <input class="register__form-text" type="text" name="password" placeholder="Пароль"/>
                    </label>
                </div>
                <div class="input">
                    <label>
                        <input class="register__form-text" type="text" name="passwordagain" placeholder="Повторите пароль"/>
                    </label>
                </div>
            </div>
            <div class="register__form-button">
                <input type="submit" value="Регистрация"/>
            </div>
        </form>
    </main>
<?php
    $link = mysqli_connect('localhost', 'root', '', 'pairs');

    if(mysqli_connect_errno()){
        echo mysqli_connect_error();
    }

    function deleteChars($optiontask) {
        $index = 0;
        while($optiontask[$index] != '"') {
            $index++;
        }
        $optiontask = trim(substr($optiontask, $index));
        $index = 1;
        while($optiontask[$index] != '"') {
            $index++;
        }
        $optiontask = trim(substr($optiontask, 1, $index - 1));
        return $optiontask;
    }

    $link = mysqli_connect('localhost', 'root', '', 'pairs');

    if(mysqli_connect_errno()){
        echo mysqli_connect_error();
    }
    ob_start();
    var_dump($_GET['nickname']);
    $nickname = ob_get_clean();
    $nickname = strip_tags($nickname);
    $nickname = deleteChars($nickname);

    ob_start();
    var_dump($_GET['email']);
    $email = ob_get_clean();
    $email = strip_tags($email);
    $email = deleteChars($email);

    ob_start();
    var_dump($_GET['age']);
    $age = ob_get_clean();
    $age = strip_tags($age);
    $age = deleteChars($age);

    ob_start();
    var_dump($_GET['sex']);
    $sex = ob_get_clean();
    $sex = strip_tags($sex);
    $sex = deleteChars($sex);

    ob_start();
    var_dump($_GET['password']);
    $password = ob_get_clean();
    $password = strip_tags($password);
    $password = deleteChars($password);

    ob_start();
    var_dump($_GET['passwordagain']);
    $passwordagain = ob_get_clean();
    $passwordagain = strip_tags($passwordagain);
    $passwordagain = deleteChars($passwordagain);

    $b = ($password === $passwordagain);

    if ($b) {
        $query = "INSERT INTO usersdetails (id, nickname, email, password) VALUES (\"\", \"$nickname\", \"$email\", \"$password\")";
        $result = mysqli_query($link, $query);

        $query = "INSERT INTO users (user_name, age, likes, description, admin) VALUES (\"$nickname\", \"$age\", 0, \"nullllll\", 0)";
        $result = mysqli_query($link, $query);

        $result2 = mysqli_query($link, "SELECT id_user FROM users WHERE user_name = '$nickname'");
        $index = 0;
        $id_users = [];
        while($row = mysqli_fetch_array($result2))
        {
            $id_users[$index] = $row["id_user"];
            $index++;
        }
        $id_user = $id_users[0];

        $query = "INSERT INTO usersgenders (id_user, id_gender) VALUES (\"$id_user\", ";
        if ($sex == "мужской") {
            $query .= "0)";
        } elseif ($sex == "женский") {
            $query .= "1)";
        } elseif ($sex == "бойчик") {
            $query .= "3)";
        }

        $result = mysqli_query($link, $query);
    }

    mysqli_close($link);
?>
<script>
    if (<?php echo $b; ?>) {
        window.location.replace("authpage.php");
    }
</script>
<?php
    include("footer.html");
    include("layoutfoot.html"); 
?>
