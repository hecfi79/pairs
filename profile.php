<?php
    $b = 0;
    if (!isset($_COOKIE["auth"])) {
        $b = 1;
    }
?>
<script>
    if (<?php echo $b; ?>) {
        window.location.replace("403.php");
    }
</script>

<?php
    include("layouthead.php");
    include("header.php");
?>
	<main class="main_hobby">
        <div class="left-block">
            <form class="input__form" enctype="multipart/form-data" action="action.php" method="POST">
                <div class="search__form-input-file">
                    <input type="file" name="picField" style="margin-top: 25px; margin-left: 10px"/>
                    <input class="load_near-text" type="submit" value="загрузить"/>
                </div>
            </form>
            <img id="outImage" style="display: none"/>
            <div class="user_left-block">
                <?php 
                    //error_reporting(E_ERROR | E_PARSE);
                    
                    $link = mysqli_connect('localhost', 'root', '', 'pairs');
                    if(mysqli_connect_errno()){  
                        echo mysqli_connect_error();  
                    }
                    $cookie = $_COOKIE["auth"];
                    $results = mysqli_query($link, "SELECT photo FROM users WHERE user_name = '$cookie'");
                    $index = 0;
                    $images = [];
                    while($row = mysqli_fetch_array($results))
                    {
                        $images[$index] = $row["photo"];
                        $index++;
                    }
                    $image = $images[0];
                    $imageAvatar = "";
                    for ($i = 0; $i < strlen($image); $i++) {
                        $imageAvatar .= $image[$i];
                    }
                    $style = "background: url(\"$imageAvatar\");";
                    echo "<img class=\"user_photo\", src='$image'></div>";
                ?>
                <div class="user_nickname">
                    <?php
                        echo $_COOKIE["auth"];
                    ?>
                </div>
            </div>
            <div class="right_block">
                <p class="user_info-block-text">Пол: 
                    <?php
                        $link = mysqli_connect('localhost', 'root', '', 'pairs');

                        if(mysqli_connect_errno()){  
                            echo mysqli_connect_error();  
                        }
                        $cookie = $_COOKIE["auth"];
                        $query = "SELECT gender_name FROM genders WHERE id_gender = (SELECT id_gender FROM usersgenders WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$cookie'))";
                        $result = mysqli_query($link, $query);
                        $index = 0;
                        $gender_name = [];
                        while($row = mysqli_fetch_array($result))
                        {
                            $gender_name[$index] = $row["gender_name"];
                            $index++;
                        }
                        echo $gender_name[0];
                        mysqli_close($link);
                    ?>
                </p>
                <p class="user_info-block-text">Возраст: 
                    <?php
                        $link = mysqli_connect('localhost', 'root', '', 'pairs');

                        if(mysqli_connect_errno()){  
                            echo mysqli_connect_error();  
                        }
                        $query = "SELECT age FROM users WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$cookie')";
                        $result = mysqli_query($link, $query);
                        $index = 0;
                        $age = [];
                        while($row = mysqli_fetch_array($result))
                        {
                            $age[$index] = $row["age"];
                            $index++;
                        }
                        echo $age[0];
                        mysqli_close($link);
                    ?>
                </p>
                <p class="user_info-block-text">Хобби: 
                    <?php
                        $link = mysqli_connect('localhost', 'root', '', 'pairs');

                        if(mysqli_connect_errno()){  
                            echo mysqli_connect_error();  
                        }
                        
                        $cookie = $_COOKIE["auth"];
                        $query = "SELECT id_hobby FROM usershobbies WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$cookie')";
                        $result = mysqli_query($link, $query);
                        $index = 0;
                        $hobbyids = [];
                        while($row = mysqli_fetch_array($result))
                        {
                            $hobbyids[$index] = $row["id_hobby"];
                            $index++;
                        }
                        $index = 0;
                        $usershobby = [];
                        for($i = 0; $i < count($hobbyids); $i++) {
                            $query = "SELECT hobby_name FROM hobbies WHERE id_hobby = $hobbyids[$i]";
                            $result = mysqli_query($link, $query);
                            while($row = mysqli_fetch_array($result))
                            {
                                $usershobby[$index] = $row["hobby_name"];
                                $index++;
                            }
                        }
                        for($i = 0; $i < count($usershobby); $i++) {
                            if ($i != count($usershobby) - 1) {
                                echo $usershobby[$i] . ", \n";
                            } else {
                                echo $usershobby[$i];
                            }
                        }
                        mysqli_close($link);
                    ?>
                </p>
                <p class="user_info-block-text">Лайки:
                    <?php
                        $link = mysqli_connect('localhost', 'root', '', 'pairs');

                        if(mysqli_connect_errno()){  
                            echo mysqli_connect_error();  
                        }
                        $cookie = $_COOKIE["auth"];
                        $query = "SELECT likes FROM users WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$cookie')";
                        $result = mysqli_query($link, $query);
                        $index = 0;
                        $likes = [];
                        while($row = mysqli_fetch_array($result))
                        {
                            $likes[$index] = $row["likes"];
                            $index++;
                        }
                        echo $likes[0];
                        mysqli_close($link);
                    ?>
                </p>
                <form class="hobby_update-left-block">
                    <div class="hobby__form-button" style="width: 240px;">
                        <input type="submit" value="Изменить!"/>
                    </div>
                    <div class="search__form-area" style="margin-top: 17px;">
                        <select class="input__search" id="selectHobby" name="taskOptionHobby">
                            <option value="0">Хобби</option>
                            <?php
                                $link = mysqli_connect('localhost', 'root', '', 'pairs');
                
                                if(mysqli_connect_errno()){  
                                    echo mysqli_connect_error();  
                                }  
                                
                                $query = "SELECT hobby_name FROM hobbies";
                                $result = mysqli_query($link, $query);
                                $index = 0;
                                $names = [];
                                while($row = mysqli_fetch_array($result))
                                {
                                    $names[$index] = $row["hobby_name"];
                                    $index++;
                                }
                                foreach($names as $name) {
                                    echo "<option>$name</option>";
                                }
                                mysqli_close($link);
                            ?>
                        </select>
                        <select class="input__search" id="selectAge" name="taskOptionAge">
                            <option value="0">Возраст</option>
                            <?php
                                for($i = 16; $i < 50; $i++) {
                                    echo "<option>$i</option>";
                                }
						    ?>
                        </select>    
                    </div>
                </form>
                <?php
                    function deleteChars($optiontask) {
                        $index = 0;
                        while($optiontask[$index] != '"') {
                            $index++;
                        }
                        $optiontask = trim(substr($optiontask, $index));
                        $index = 1;
                        while($optiontask[$index] != '"') {
                            $index++;
                        }
                        $optiontask = trim(substr($optiontask, 1, $index - 1));
                        return trim($optiontask);	
                    }
                    
                    if(isset($_GET['taskOptionHobby']) && isset($_GET['taskOptionAge'])) {
                        ob_start();
                        var_dump($_GET['taskOptionHobby']);
                        $taskOptionHobby = ob_get_clean();
                        $taskOptionHobby = deleteChars($taskOptionHobby);

                        ob_start();
                        var_dump($_GET['taskOptionAge']);
                        $taskOptionAge = ob_get_clean();
                        $taskOptionAge = deleteChars($taskOptionAge);

                        $link = mysqli_connect('localhost', 'root', '', 'pairs');
                        if(mysqli_connect_errno()){  
                            echo mysqli_connect_error();  
                        }
                        $a = 0;
                        $b = 0;
                        $c = 0;
                        if ($taskOptionHobby !== "0" && $taskOptionAge === "0") {
                            $query = "SELECT id_hobby FROM hobbies WHERE hobby_name = '$taskOptionHobby'";
                            $result = mysqli_query($link, $query);
                            $index = 0;
                            $hobbies_ids = [];
                            while($row = mysqli_fetch_array($result))
                            {
                                $hobbies_ids[$index] = $row["id_hobby"];
                                $index++;
                            }
                            $cookie = $_COOKIE["auth"];
                            $query = "SELECT id_user FROM users WHERE user_name = '$cookie'";
                            $result = mysqli_query($link, $query);
                            $index = 0;
                            $names = [];
                            while($row = mysqli_fetch_array($result))
                            {
                                $names[$index] = $row["id_user"];
                                $index++;
                            }
                            $query = "INSERT INTO usershobbies (id_user, id_hobby) VALUES ($names[0], $hobbies_ids[0])";
                            $result = mysqli_query($link, $query);
                            $a = 1;
                        }
                        if ($taskOptionAge !== "0" && $taskOptionHobby === "0") {
                            $query = "SELECT id_hobby FROM hobbies WHERE hobby_name = '$taskOptionHobby'";
                            $result = mysqli_query($link, $query);
                            $index = 0;
                            $hobbies_ids = [];
                            while($row = mysqli_fetch_array($result))
                            {
                                $hobbies_ids[$index] = $row["id_hobby"];
                                $index++;
                            }
                            $cookie = $_COOKIE["auth"];
                            $query = "SELECT id_user FROM users WHERE user_name = '$cookie'";
                            $result = mysqli_query($link, $query);
                            $index = 0;
                            $names = [];
                            while($row = mysqli_fetch_array($result))
                            {
                                $names[$index] = $row["id_user"];
                                $index++;
                            }
                            $query = "UPDATE users SET age = $taskOptionAge WHERE user_name = '$cookie'";
                            $result = mysqli_query($link, $query);
                            $b = 1;
                        }
                        if ($taskOptionAge !== "0" && $taskOptionHobby !== "0") {
                            $query = "SELECT id_hobby FROM hobbies WHERE hobby_name = '$taskOptionHobby'";
                            $result = mysqli_query($link, $query);
                            $index = 0;
                            $hobbies_ids = [];
                            while($row = mysqli_fetch_array($result))
                            {
                                $hobbies_ids[$index] = $row["id_hobby"];
                                $index++;
                            }
                            $cookie = $_COOKIE["auth"];
                            $query = "SELECT id_user FROM users WHERE user_name = '$cookie'";
                            $result = mysqli_query($link, $query);
                            $index = 0;
                            $names = [];
                            while($row = mysqli_fetch_array($result))
                            {
                                $names[$index] = $row["id_user"];
                                $index++;
                            }
                            $query = "INSERT INTO usershobbies (id_user, id_hobby) VALUES ($names[0], $hobbies_ids[0])";
                            $result = mysqli_query($link, $query);
                            $query2 = "UPDATE users SET age = $taskOptionAge WHERE user_name = '$cookie'";
                            $result2 = mysqli_query($link, $query2);
                            $c = 1;
                        }
                    }
                ?>
                <script>
                    if (<?php echo $a; ?> || <?php echo $b; ?> || <?php echo $c; ?>) {
                        window.location.replace("profile.php");
                    }
                </script>
            </div>    
        </div>
	</main>
<?php
    include("footer.html");
    include("layoutfoot.html");
?>