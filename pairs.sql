-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 12 2023 г., 14:37
-- Версия сервера: 10.4.27-MariaDB
-- Версия PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `pairs`
--

-- --------------------------------------------------------

--
-- Структура таблицы `genders`
--

CREATE TABLE `genders` (
  `id_gender` int(11) NOT NULL,
  `gender_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Дамп данных таблицы `genders`
--

INSERT INTO `genders` (`id_gender`, `gender_name`) VALUES
(0, 'мужской'),
(1, 'женский'),
(6, 'бойчик');

-- --------------------------------------------------------

--
-- Структура таблицы `hobbies`
--

CREATE TABLE `hobbies` (
  `id_hobby` int(11) NOT NULL,
  `hobby_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Дамп данных таблицы `hobbies`
--

INSERT INTO `hobbies` (`id_hobby`, `hobby_name`) VALUES
(1, 'математика'),
(2, 'рисование'),
(3, 'чтение'),
(4, 'оригами'),
(5, 'программирование'),
(9, 'китайский');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `likes` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `photo` varchar(500) DEFAULT 'images/user.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id_user`, `user_name`, `age`, `likes`, `description`, `admin`, `photo`) VALUES
(27, 'admin', 30, 3, 'nullllll', 1, 'images/6477c51b07666_kaioura-cute-kaioura.gif'),
(28, 'Rusya', 21, 0, 'nullllll', 0, 'images/6486e232df9c7_1_edited_comics_page123123123 (3).png'),
(29, 'Alexander', 20, 1, '', 0, 'images/user.png'),
(30, 'Alya', 20, 1, 'nulll', 0, 'images/user.png'),
(31, 'Nastya', 18, 0, 'nulll', 0, 'images/user.png'),
(32, 'Darya', 19, 1, 'nulll', 0, 'images/user.png'),
(33, 'Alice', 21, 1, 'nulll', 0, 'images/user.png'),
(34, 'Alexandra', 19, 0, 'nulll', 0, 'images/user.png'),
(35, 'Kirill', 20, 1, 'nulll', 0, 'images/user.png'),
(62, 'Arch', 21, 0, 'nullllll', 0, 'images/6477b00ed67cd_3fb6438d82e99695a4e63beba426c750.jpg'),
(63, 'Алина', 22, 0, 'nulll', 0, 'images/user.png'),
(64, 'Лена', 16, 0, 'nulll', 0, 'images/user.png'),
(65, 'Антон', 24, 0, 'nulll', 0, 'images/user.png'),
(66, 'Константин', 34, 0, 'nulll', 0, 'images/user.png'),
(67, 'Глеб', 28, 0, 'nulll', 0, 'images/user.png'),
(68, 'Денис', 26, 0, 'nulll', 0, 'images/user.png'),
(69, 'Алиса', 20, 0, 'nullllll', 0, 'images/64782239b9fe7_20a633d501ce63a63880de219d1a7ab0.jpeg'),
(70, 'Usernew', 24, 0, 'nulll', 0, 'images/user.png');

-- --------------------------------------------------------

--
-- Структура таблицы `usersdetails`
--

CREATE TABLE `usersdetails` (
  `id` int(11) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Дамп данных таблицы `usersdetails`
--

INSERT INTO `usersdetails` (`id`, `nickname`, `email`, `password`) VALUES
(27, 'admin', 'admin@mail.ru', 'admin'),
(28, 'Ruslan', 'ruslan@mail.ru', 'Ruslan'),
(31, 'Arch', 'arch@mail.ru', 'arch'),
(32, 'Алиса', 'alice@mail.ru', 'alice');

-- --------------------------------------------------------

--
-- Структура таблицы `usersgenders`
--

CREATE TABLE `usersgenders` (
  `id_user` int(11) NOT NULL,
  `id_gender` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Дамп данных таблицы `usersgenders`
--

INSERT INTO `usersgenders` (`id_user`, `id_gender`) VALUES
(27, 0),
(28, 0),
(29, 0),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 0),
(48, 0),
(52, 0),
(55, 1),
(56, 3),
(57, 3),
(59, 1),
(61, 3),
(62, 3),
(63, 1),
(64, 1),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 1),
(70, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `usershobbies`
--

CREATE TABLE `usershobbies` (
  `id_user` int(11) NOT NULL,
  `id_hobby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Дамп данных таблицы `usershobbies`
--

INSERT INTO `usershobbies` (`id_user`, `id_hobby`) VALUES
(1, 3),
(1, 5),
(27, 1),
(27, 3),
(27, 4),
(27, 5),
(28, 1),
(28, 2),
(28, 3),
(28, 4),
(28, 5),
(28, 9),
(42, 5),
(46, 5),
(48, 5),
(52, 1),
(55, 5),
(56, 4),
(57, 4),
(59, 1),
(61, 1),
(63, 2),
(64, 5),
(65, 5),
(66, 4),
(67, 4),
(68, 1),
(70, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `userslikes`
--

CREATE TABLE `userslikes` (
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_liked` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Дамп данных таблицы `userslikes`
--

INSERT INTO `userslikes` (`user_name`, `user_liked`) VALUES
('залупа', 'r6'),
('залупа', 'Rus'),
('admin', 'Alexander'),
('admin', 'Alya'),
('admin', 'Kirill'),
('admin', 'Alice'),
('Ruslan', 'admin'),
('admin', 'Darya'),
('admin', 'admin'),
('Алиса', 'admin');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id_gender`);

--
-- Индексы таблицы `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id_hobby`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Индексы таблицы `usersdetails`
--
ALTER TABLE `usersdetails`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `usersgenders`
--
ALTER TABLE `usersgenders`
  ADD PRIMARY KEY (`id_user`,`id_gender`);

--
-- Индексы таблицы `usershobbies`
--
ALTER TABLE `usershobbies`
  ADD PRIMARY KEY (`id_user`,`id_hobby`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `genders`
--
ALTER TABLE `genders`
  MODIFY `id_gender` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id_hobby` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT для таблицы `usersdetails`
--
ALTER TABLE `usersdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
