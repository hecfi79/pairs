<?php
    include("layouthead.php");
    include("header.php");
?>
	<main class="main">
		<form class="input__form" enctype="multipart/form-data" action="action.php" method="POST">
            <div class="search__form-input-file">
            <input class="load" type="file" id="picField" name="picField"/>
            <input class="load_near-text" type="submit" value="загрузить"/>
		</form>
		<img id="outImage" style="display: none"/>
		<?php
			$link = mysqli_connect('localhost', 'root', '', 'pairs');
			if(mysqli_connect_errno()){  
				echo mysqli_connect_error();  
			}
			$cookie = $_COOKIE["auth"];
			$results = mysqli_query($link, "SELECT photo FROM users WHERE user_name = '$cookie'");
			$index = 0;
			$images = [];
			while($row = mysqli_fetch_array($results))
			{
				$images[$index] = $row["photo"];
				$index++;
			}
			$image = $images[0];
		?>
		</div>
		<div class="user_left-block">
			<div class="user_nickname">
				<?php
					echo $_COOKIE["auth"];
				?>
			</div>
			<?php 
				$imageAvatar = "";
				for ($i = 0; $i < strlen($image); $i++) {
					$imageAvatar .= $image[$i];
				}
				$style = "background: url(\"$imageAvatar\");";
				echo "<div class=\"user_photo\", style='$style'></div>";
			?>
		</div>
		<div class="user_info-block">
			<div class="user_info-block-text">
				Пол: 
				<?php
					$link = mysqli_connect('localhost', 'root', '', 'pairs');

					if(mysqli_connect_errno()){  
						echo mysqli_connect_error();  
					}
					$cookie = $_COOKIE["auth"];
					$query = "SELECT gender_name FROM genders WHERE id_gender = (SELECT id_gender FROM usersgenders WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$cookie'))";
					$result = mysqli_query($link, $query);
					$index = 0;
					$gender_name = [];
					while($row = mysqli_fetch_array($result))
					{
						$gender_name[$index] = $row["gender_name"];
						$index++;
					}
					echo $gender_name[0];
					mysqli_close($link);
				?>
			</div>
			<div class="user_info-block-text">
				Возраст: 
				<?php
					$link = mysqli_connect('localhost', 'root', '', 'pairs');

					if(mysqli_connect_errno()){  
						echo mysqli_connect_error();  
					}
					$query = "SELECT age FROM users WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$cookie')";
					$result = mysqli_query($link, $query);
					$index = 0;
					$age = [];
					while($row = mysqli_fetch_array($result))
					{
						$age[$index] = $row["age"];
						$index++;
					}
					echo $age[0];
					mysqli_close($link);
				?>
			</div>
			<div class="user_info-block-text">
				Хобби:
				<?php
					$link = mysqli_connect('localhost', 'root', '', 'pairs');

					if(mysqli_connect_errno()){  
						echo mysqli_connect_error();  
					}
					
					$cookie = $_COOKIE["auth"];
					$query = "SELECT id_hobby FROM usershobbies WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$cookie')";
					$result = mysqli_query($link, $query);
					$index = 0;
					$hobbyids = [];
					while($row = mysqli_fetch_array($result))
					{
						$hobbyids[$index] = $row["id_hobby"];
						$index++;
					}
					$index = 0;
					$usershobby = [];
					for($i = 0; $i < count($hobbyids); $i++) {
						$query = "SELECT hobby_name FROM hobbies WHERE id_hobby = $hobbyids[$i]";
						$result = mysqli_query($link, $query);
						while($row = mysqli_fetch_array($result))
						{
							$usershobby[$index] = $row["hobby_name"];
							$index++;
						}
					}
					for($i = 0; $i < count($usershobby); $i++) {
						if ($i != count($usershobby) - 1) {
							echo $usershobby[$i] . ", ";
						} else {
							echo $usershobby[$i];
						}
					}
					mysqli_close($link);
				?>
			</div>
			<div class="user_info-block-text">
				Лайки: 
				<?php
					$link = mysqli_connect('localhost', 'root', '', 'pairs');

					if(mysqli_connect_errno()){  
						echo mysqli_connect_error();  
					}
					$cookie = $_COOKIE["auth"];
					$query = "SELECT likes FROM users WHERE id_user = (SELECT id_user FROM users WHERE user_name = '$cookie')";
					$result = mysqli_query($link, $query);
					$index = 0;
					$likes = [];
					while($row = mysqli_fetch_array($result))
					{
						$likes[$index] = $row["likes"];
						$index++;
					}
					echo $likes[0];
					mysqli_close($link);
				?>
			</div>
		</div>
	</main>
<?php
    include("footer.html");
    include("layoutfoot.html");
?>