<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Pairs</title>
  <link rel="stylesheet" href="styles/mainpage.css">
  <link rel="stylesheet" href="styles/profilepage.css">
  <link rel="stylesheet" href="styles/searchpage.css">
  <link rel="stylesheet" href="styles/header.css">
  <link rel="stylesheet" href="styles/footer.css">
  <link rel="stylesheet" href="styles/index.css">
  <link rel="stylesheet" href="styles/registerpage.css">
  <link rel="stylesheet" href="styles/authpage.css">
  <link rel="stylesheet" href="styles/admin.css">
  <link rel="stylesheet" href="styles/404.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <style>
    <?php
        include("styles/mainpage.css");
        include("styles/profilepage.css");
        include("../styles/profilepage.css");
        include("styles/searchpage.css");
        include("styles/header.css");
        include("styles/footer.css");
        include("styles/index.css");
        include("../styles/index.css");
        include("styles/registerpage.css");
        include("styles/authpage.css");
        include("styles/404.css");
        include("../styles/404.css");
        include("styles/admin.css");
        include("../styles/admin.css");
    ?>
  </style>
</head>
<body>