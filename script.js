function resetButtonBind() {
    $(document).on(
		'click', 
		'.reset_button', 
		function() {
			$(this).parents('form').reset()
		}
	)
}

$(document).ready(function(){
  
	$(".fa-search").click(function(){
		$(".wrap, .myInput").toggleClass("active");
		$("input[type='text']").focus();
	});

});