<?php
    $b = 0;
    if (!isset($_COOKIE["auth"])) {
        $b = 1;
    }
?>
<script>
    if (<?php echo $b; ?>) {
        window.location.replace("403.php");
    }
</script>

<?php
    include("layouthead.php");
    include("header.php");
?>

	<main class="main__search" style="max-height: 2300px;
        height: 170%;">
		<div>
			<form class="main__search-left-block">
				<div class="search__form-button">
					<input type="submit" value="Искать!"/>
				</div>
				<div class="register__form-area-2" style="display: flex; flex-direction: column;">
					<select class="input__search" style="margin-top: 40px" id="selectGender" name="taskOptionGender">
						<option value="0">Пол</option>
						<?php
							$link = mysqli_connect('localhost', 'root', '', 'pairs');
			
							if(mysqli_connect_errno()) {  
								echo mysqli_connect_error();  
							}
							
							$query = "SELECT gender_name FROM genders";
							$result = mysqli_query($link, $query);
							$index = 0;
							$genders = [];
							while($row = mysqli_fetch_array($result))
							{
								$genders[$index] = $row["gender_name"];
								$index++;
							}
							foreach($genders as $gender) {
								echo "<option>$gender</option>";
							}
							mysqli_close($link);
						?>
					</select>
					<select class="input__search" id="selectAge" name="taskOptionAge">
						<option value="0">Возраст</option>
						<?php
							for($i = 16; $i < 50; $i++) {
								echo "<option>$i</option>";
							}
						?>
					</select>
					<select class="input__search" id="selectHobby" name="taskOptionHobby">
						<option value="0">Хобби</option>
						<?php
							$link = mysqli_connect('localhost', 'root', '', 'pairs');
			
							if(mysqli_connect_errno()){  
								echo mysqli_connect_error();  
							}  
							
							$query = "SELECT hobby_name FROM hobbies";
							$result = mysqli_query($link, $query);
							$index = 0;
							$names = [];
							while($row = mysqli_fetch_array($result))
							{
								$names[$index] = $row["hobby_name"];
								$index++;
							}
							foreach($names as $name) {
								echo "<option>$name</option>";
							}
							mysqli_close($link);
						?>
					</select>
					<button type="reset" id="filter_reset" class="reset__button">Сбросить</button>
				</div>
			</form>
			<div class="recomendations">
				<h1 class="recomendations__text">Рекомендации</h1>
                <?php
                    $link = mysqli_connect('localhost', 'root', '', 'pairs');

                    if(mysqli_connect_errno()){
                        echo mysqli_connect_error();
                    }

                    $cookie = $_COOKIE["auth"];

                    $query = "SELECT gender_name FROM genders WHERE id_gender = (SELECT id_gender FROM usersgenders WHERE id_user = (SELECT id_user FROM users WHERE user_name = \"$cookie\"))";
                    $result = mysqli_query($link, $query);
                    $index = 0;
                    $gender_name = [];
                    while($row = mysqli_fetch_array($result))
                    {
                        $gender_name[$index] = $row["gender_name"];
                        $index++;
                    }

                    $query = "SELECT user_name, likes FROM users, usersgenders WHERE users.id_user = usersgenders.id_user AND usersgenders.id_gender = ";

                    if ($gender_name[0] == "мужской") {
                        $query .= " 1 ORDER BY likes DESC";
                    } elseif ($gender_name[0] == "женский") {
                        $query .= " 0 ORDER BY likes DESC";
                    }

                    $result = mysqli_query($link, $query);
                    $index = 0;
                    $usernames = [];
                    $likes = [];
                    while($row = mysqli_fetch_array($result))
                    {
                        $usernames[$index] = $row["user_name"];
                        $likes[$index] = $row["likes"];
                        $index++;
                    }

                    for($i = 0; $i < 7; $i++) {
                        echo "<div class=\"user__info\">";
                        echo "<a href=\"../profileother.php/$usernames[$i]\"><img src=\"images/user.png\" height=\"30\" width=\"28.6\"/></a>";
                        echo "<a href=\"../profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
                        echo "<a href=\"../profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
                        echo "</div>";
                    }
                    mysqli_close($link);
                ?>
			</div>
			<div class="recomendations" style="margin-bottom: 100px;">
				<h1 class="recomendations__text">Рейтинг</h1>
                <?php
                    $link = mysqli_connect('localhost', 'root', '', 'pairs');

                    if(mysqli_connect_errno()){
                        echo mysqli_connect_error();
                    }

                    $query = "SELECT user_name, likes FROM users ORDER BY likes DESC";
                    $result = mysqli_query($link, $query);
                    $index = 0;
                    $usernames = [];
                    $likes = [];
                    while($row = mysqli_fetch_array($result))
                    {
                        $usernames[$index] = $row["user_name"];
                        $likes[$index] = $row["likes"];
                        $index++;
                    }
                    for($i = 0; $i < 7; $i++) {
                        echo "<div class=\"user__info\">";
                        echo "<a href=\"../profileother.php/$usernames[$i]\"><img src=\"images/user.png\" height=\"30\" width=\"28.6\"/></a>";
                        echo "<a href=\"../profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
                        echo "<a href=\"../profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
                        echo "</div>";
                    }
                    mysqli_close($link);
                ?>
			</div>
		</div>
		<div class="main__search-right-block">
			<div class="wrap">
				<form class="search__form">
					<!--<div class="search__form-input">-->
						<input type="text" class="myInput" name="name" id="name">
					<!--</div>-->
				</form>
				<i class="fa fa-search" aria-hidden="true"></i>
			</div>
			<div class="main__block-rectangle">
			
				<?php
					function deleteChars($optiontask) {
						$index = 0;
						while($optiontask[$index] != '"') {
							$index++;
						}
						$optiontask = trim(substr($optiontask, $index));
						$index = 1;
						while($optiontask[$index] != '"') {
							$index++;
						}
						$optiontask = trim(substr($optiontask, 1, $index - 1));
						return $optiontask;	
					}
				
					$link = mysqli_connect('localhost', 'root', '', 'pairs');

					if(mysqli_connect_errno()){  
						echo mysqli_connect_error();  
					}
					ob_start();
					var_dump($_GET['taskOptionGender']);
					$taskOptionGender = ob_get_clean();
					$taskOptionGender = strip_tags($taskOptionGender);
					$taskOptionGender = deleteChars($taskOptionGender);
					
					ob_start();
					var_dump($_GET['taskOptionAge']);
					$taskOptionAge = ob_get_clean();
					$taskOptionAge = strip_tags($taskOptionAge);
					$taskOptionAge = deleteChars($taskOptionAge);
					
					ob_start();
					var_dump($_GET['taskOptionHobby']);
					$taskOptionHobby = ob_get_clean();
					$taskOptionHobby = strip_tags($taskOptionHobby);
					$taskOptionHobby = deleteChars($taskOptionHobby);

					ob_start();
					var_dump($_GET['name']);
					$search = ob_get_clean();
					$search = strip_tags($search);
					$search = deleteChars($search);

					if ($search != "") {
						$query = "SELECT user_name, likes FROM users WHERE user_name LIKE '%$search%' OR user_name LIKE '%$search' OR user_name LIKE '$search%'";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}

						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							for($j = 0; $j < 2; $j++) {
								if ($i + $j < count($usernames)) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									$un = $usernames[$idx];
									$lk = $likes[$idx];
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$un\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$un\"><p class=\"user-info-text\">$un</p></a>";
									echo "<a href=\"profileother.php/$un\"><p class=\"user-info-text\">$lk</p></a>";
									echo "</div>";
								}
							}
							echo "</div>";
						}						
					} else {
						$query = "SELECT user_name, likes FROM users";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
					
					if ($taskOptionGender === "0" and $taskOptionAge === "0" and $taskOptionHobby === "0") {
						$query = "SELECT user_name, likes FROM users";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
					elseif ($taskOptionGender !== "0" and $taskOptionAge === "0" and $taskOptionHobby === "0") {
						$query = "SELECT user_name, likes FROM users, usersgenders,
						genders WHERE users.id_user = usersgenders.id_user
						AND genders.id_gender = usersgenders.id_gender
						AND genders.gender_name = \"$taskOptionGender\"";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
					elseif ($taskOptionGender === "0" and $taskOptionAge !== "0" and $taskOptionHobby === "0") {
						$query = "SELECT user_name, likes FROM users WHERE users.age = \"$taskOptionAge\"";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
					elseif ($taskOptionGender === "0" and $taskOptionAge === "0" and $taskOptionHobby !== "0") {
						$query = "SELECT user_name, likes FROM users, usershobbies,
						hobbies WHERE users.id_user = usershobbies.id_user
						AND hobbies.id_hobby = usershobbies.id_hobby
						AND hobbies.hobby_name = \"$taskOptionHobby\"";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
					elseif ($taskOptionGender !== "0" and $taskOptionAge !== "0" and $taskOptionHobby === "0") {
						$query = "SELECT user_name, likes FROM users, usersgenders,
						genders WHERE users.id_user = usersgenders.id_user
						AND genders.id_gender = usersgenders.id_gender
						AND genders.gender_name = \"$taskOptionGender\" AND users.age = \"$taskOptionAge\"";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
					elseif ($taskOptionGender !== "0" and $taskOptionAge === "0" and $taskOptionHobby !== "0") {
						$query = "SELECT user_name, likes FROM users, usersgenders, usershobbies,
						genders, hobbies WHERE users.id_user = usershobbies.id_user AND
						users.id_user = usersgenders.id_user
						AND genders.id_gender = usersgenders.id_gender AND
						hobbies.id_hobby = usershobbies.id_hobby
						AND genders.gender_name = \"$taskOptionGender\" AND hobbies.hobby_name = \"$taskOptionHobby\"";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
					elseif ($taskOptionGender !== "0" and $taskOptionAge === "0" and $taskOptionHobby !== "0") {
						$query = "SELECT user_name, likes FROM users, usersgenders, usershobbies,
						genders, hobbies WHERE users.id_user = usershobbies.id_user AND
						users.id_user = usersgenders.id_user
						AND genders.id_gender = usersgenders.id_gender AND
						hobbies.id_hobby = usershobbies.id_hobby
						AND genders.gender_name = \"$taskOptionGender\" AND hobbies.hobby_name = \"$taskOptionHobby\"";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
					elseif ($taskOptionGender === "0" and $taskOptionAge !== "0" and $taskOptionHobby !== "0") {
						$query = "SELECT user_name, likes FROM users, usershobbies,
						hobbies WHERE users.id_user = usershobbies.id_user
						AND	hobbies.id_hobby = usershobbies.id_hobby
						AND users.age = \"$taskOptionAge\" AND hobbies.hobby_name = \"$taskOptionHobby\"";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
					elseif ($taskOptionGender !== "0" and $taskOptionAge !== "0" and $taskOptionHobby !== "0") {
						$query = "SELECT user_name, likes FROM users, usershobbies, usersgenders,
						hobbies, genders WHERE users.id_user = usershobbies.id_user 
						AND	users.id_user = usersgenders.id_user
						AND hobbies.id_hobby = usershobbies.id_hobby 
						AND genders.id_gender = usersgenders.id_gender
						AND hobbies.hobby_name = \"$taskOptionHobby\" AND genders.gender_name = \"$taskOptionGender\"
						AND users.age = \"$taskOptionAge\"";
						$result = mysqli_query($link, $query);
						$usernames = [];
						$likes = [];
						$index = 0;
						while($row = mysqli_fetch_array($result))
						{
							$likes[$index] = $row["likes"];
							$usernames[$index] = $row["user_name"];
							$index++;
						}
						for($i = 0; $i < count($usernames); $i+=2) {
							echo "<div class=\"main__block-user-info\">";
							if ($i + 1 < count($usernames)) {
								for($j = 0; $j < 2; $j++) {
									$idx = $i + $j;
									if ($idx > 0 && $usernames[$idx] == $usernames[$idx-1]) {
										continue; // пропускаем повторение
									}
									echo "<div class=\"user__info-right-block\">";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$usernames[$idx]</p></a>";
									echo "<a href=\"profileother.php/$usernames[$idx]\"><p class=\"user-info-text\">$likes[$idx]</p></a>";
									echo "</div>";
								}
							} else if ($i + 1 < count($usernames)){
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$i]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$usernames[$i]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$i]\"><p class=\"user-info-text\">$likes[$i]</p></a>";
								echo "</div>";
								$one = $i + 1;
								if ($one > 0 && $usernames[$one] == $usernames[$one-1]) {
									continue; // пропускаем повторение
								}
								echo "<div class=\"user__info-right-block\">";
								echo "<a href=\"profileother.php/$usernames[$one]\"><img src=\"images/user.png\" width=\"28.6px\" height=\"30px\"/></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$usernames[$one]</p></a>";
								echo "<a href=\"profileother.php/$usernames[$one]\"><p class=\"user-info-text\">$likes[$one]</p></a>";
								echo "</div>";
							}
							echo "</div>";
						}
					}
				?>
			</div>
		</div>
        <!--<form class="input__form">
            <div class="search__form-input">
            <input type="file" id="picField"/>
            <input type="submit" value="загрузить"/>
            </div>
        </form>
        <img id="outImage"/>-->
	</main>
	<script>
		$(document).ready(function(){
  
		  $(".fa-search").click(function(){
		    $(".wrap, .myInput").toggleClass("active");
		    $("input[type='text']").focus();
		  });
  
		});
	
		setTimeout(function(){
			document.getElementById('preloader').style.display = 'none';
		}, 1000);
	</script>
<?php 
	include("footer.html");
	include("layoutfoot.html"); 
?>